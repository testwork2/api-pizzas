from flask import Flask, jsonify, request, Response
from flask_pymongo import PyMongo
from bson import json_util
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.config['MONGO_URI'] = 'mongodb://localhost:27017/pizzas'
mongo = PyMongo(app)

#######################################################################################################
# Metodos para la creacion y consulta de ingredientes de pizzas
#######################################################################################################
@app.route('/ingredients', methods=['POST'])
def create_ingredient():
    # Receiving Data
    name = request.json['name']
    price = request.json['price']

    if name and price:
        id = mongo.db.ingredients.insert(
            {'name': name, 'price': price})
        response = jsonify({
            '_id': str(id),
            'name': name,
            'price': price
        })
        response.status_code = 201
        return response
    else:
        return not_found()


@app.route('/ingredients', methods=['GET'])
def get_ingredients():
    ingredients = mongo.db.ingredients.find()
    response = json_util.dumps(ingredients)
    return Response(response, mimetype="application/json")

#######################################################################################################
# Metodos para la creacion y consulta de ordenes de pizzas 
#######################################################################################################
@app.route('/orders', methods=['POST'])
def create_order():
    # Receiving Data
    print(f'Lo que llega {request.json}')
    ingredients = []
    pizza_name = request.json['pizza_name']
    pizza_price = request.json['pizza_price']
    name_client = request.json['name_client']
    phone_client = request.json['phone_client']
    ingredients = request.json['ingredients']
    order_date = request.json['order_date']
    ingredients_list = request.json['ingredients_list']

    if pizza_name and pizza_price and name_client and phone_client and ingredients and order_date:
        id = mongo.db.orders.insert({ 
            'pizza_name': pizza_name, 
            'pizza_price': pizza_price, 
            'name_client': name_client, 
            'phone_client': phone_client, 
            'ingredients': ingredients, 
            'order_date': order_date,
            'ingredients_list': ingredients_list
            })
        response = jsonify({
            '_id': str(id),
            'pizza_name': pizza_name, 
            'pizza_price': pizza_price, 
            'name_client': name_client, 
            'phone_client': phone_client, 
            'ingredients': ingredients, 
            'order_date': order_date
        })
        response.status_code = 201
        return response
    else:
        return not_found()


@app.route('/orders', methods=['GET'])
def get_orders():
    orders = mongo.db.orders.find()
    response = json_util.dumps(orders)
    return Response(response, mimetype="application/json")

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'message': 'Resource Not Found ' + request.url,
        'status': 404
    }
    response = jsonify(message)
    response.status_code = 404
    return response


if __name__ == "__main__":
    app.run(debug=True)